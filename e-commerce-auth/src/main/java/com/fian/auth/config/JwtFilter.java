package com.fian.auth.config;

import com.fian.auth.service.impl.MyUserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private MyUserDetailsServiceImpl userDetailsService;

    private static final String AUTH_HEADER_PARAMETER_AUTHORIZATION = "Authorization";

    private static final String AUTH_HEADER_PARAMETER_BEARER = "Bearer ";

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.addHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
        httpServletResponse.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Auth-Token, Content-Type, Accept, Authorization");
        httpServletResponse.addHeader("Access-Control-Max-Age", "86400");
        String authorizationHeader = httpServletRequest.getHeader(AUTH_HEADER_PARAMETER_AUTHORIZATION);

        if (ObjectUtils.isEmpty(authorizationHeader)
                || !(authorizationHeader.startsWith(AUTH_HEADER_PARAMETER_BEARER))) {
            filterChain.doFilter(httpServletRequest,httpServletResponse);
            return;
        }
        String token = authorizationHeader.replace(AUTH_HEADER_PARAMETER_BEARER,"");
        String userName = jwtUtil.extractUsername(token);
        if (!(userName != null && SecurityContextHolder.getContext().getAuthentication() == null)) {
            filterChain.doFilter(httpServletRequest,httpServletResponse);
            return;
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

        if (jwtUtil.validateToken(token, userDetails)) {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            usernamePasswordAuthenticationToken
                    .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
