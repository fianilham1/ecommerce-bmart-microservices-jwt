package com.fian.transaction.service;

import com.fian.shared.data.entity.web_query.ProductCategoryParamsQuery;
import com.fian.shared.data.entity.ProductCategory;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductCategoryService {

    List<ProductCategory> addProductCategories(List<ProductCategory> productCategoryList);

    Page<ProductCategory> getAllProductCategory(ProductCategoryParamsQuery productCategoryParamsQuery);

    ProductCategory getProductCategory(String id);

    void deleteProductCategory (String id);

    ProductCategory updateProductCategory (ProductCategory updatedProductCategory);

}
