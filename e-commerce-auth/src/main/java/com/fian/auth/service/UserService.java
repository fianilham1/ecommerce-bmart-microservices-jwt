package com.fian.auth.service;

import com.fian.shared.data.entity.Users;

public interface UserService {

    Users create(Users users);

    Users update(Users users);

    Users login(Users users);

    Users loginGoogle(Users users);

    String forgotPassword(String username);

    void resetPassword(Users users);

    Users getUser(Users users);

    Users getUser(String username);

}
