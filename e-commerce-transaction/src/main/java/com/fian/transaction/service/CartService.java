package com.fian.transaction.service;


import com.fian.shared.data.entity.Cart;
import com.fian.shared.data.entity.CartItems;

public interface CartService {

    Cart addItemToCart(CartItems cartItems, String userName, String options);

    void deleteItemFromCart(String cartItemsId, String userName);

    Cart getCartCustomer(String userName);

}
