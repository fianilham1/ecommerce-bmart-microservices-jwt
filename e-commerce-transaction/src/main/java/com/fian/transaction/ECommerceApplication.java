package com.fian.transaction;

import com.fian.shared.PersistentAutoConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@SpringBootApplication(scanBasePackages = { "com.fian.transaction", "com.fian.shared.data.entity",
		"com.fian.shared.repository" })
@Import({ PersistentAutoConfig.class })
@EnableFeignClients
public class ECommerceApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(ECommerceApplication.class, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
