package com.fian.shared.repository;

import com.fian.shared.data.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query("select c from Cart c where c.users.username = :userName")
    Optional<Cart> findByUsersAndMarkForDeleteIsFalse(@Param("userName") String userName);
}
