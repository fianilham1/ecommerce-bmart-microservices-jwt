package com.fian.transaction.interceptor;

import com.blibli.oss.common.response.Response;
import com.fian.shared.data.dto.user.UserDetailsDto;
import com.fian.shared.data.dto.user.ValidateTokenDto;
import com.fian.transaction.config.JwtUtil;
import com.fian.transaction.feignclient.AuthClient;
import com.fian.transaction.validation.Role;
import feign.FeignException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.lang.reflect.Method;
import java.util.Arrays;

import static com.fian.transaction.common.constant.WhitelistUrl.WHITE_LIST_GET_PREFIX_URLS;
import static com.fian.transaction.common.util.StringUtil.matchesTextInPrefixList;

@Component
public class TransactionSecurityInterceptor implements HandlerInterceptor {

	private final Logger log = LogManager.getLogger(getClass());

	private static final String AUTH_HEADER_PARAMETER_AUTHORIZATION = "Authorization";

	private static final String AUTH_HEADER_PARAMETER_BEARER = "Bearer ";

	@Autowired
	private ObjectProvider<AuthClient> authClientProvider;

	@Autowired
	private JwtUtil jwtUtil;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
		response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Auth-Token, Content-Type, Accept, Authorization");
		response.addHeader("Access-Control-Max-Age", "86400");

		String jwtAuthToken;

		log.info("[Inside PRE Handle interceptor][" + request + "]" + "[" + request.getMethod() + "]"
				+ request.getRequestURI());

		if (request.getMethod().equalsIgnoreCase("GET") && matchesTextInPrefixList(request.getServletPath(), Arrays.asList(WHITE_LIST_GET_PREFIX_URLS))) {
			return true;
		}

		try {

			// Get JWT token from header value
			String authorizationHeader = request.getHeader(AUTH_HEADER_PARAMETER_AUTHORIZATION);

			if (ObjectUtils.isEmpty(authorizationHeader)
					|| !(authorizationHeader.startsWith(AUTH_HEADER_PARAMETER_BEARER))) {
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				return false;
			}

			jwtAuthToken = request.getHeader(AUTH_HEADER_PARAMETER_AUTHORIZATION).replace(AUTH_HEADER_PARAMETER_BEARER,"");

			// Get User Details to validate role
			UserDetailsDto userDetails = jwtUtil.extractUserDetails(jwtAuthToken);

			HandlerMethod hm;
			try {
				hm = (HandlerMethod) handler;
			} catch (ClassCastException e) {
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				return false;
			}

			Method method = hm.getMethod();
			if (method.isAnnotationPresent(Role.class)) {
				Role role = method.getAnnotation(Role.class);
				if (userDetails == null || userDetails.getRole() == null || userDetails.getUsername() == null
						|| !role.name().equalsIgnoreCase(userDetails.getRole())) {
					response.setStatus(HttpStatus.UNAUTHORIZED.value());
					return false;
				}
			}

			// Feign Auth Client to check validity of JWT Token
			AuthClient authClient = authClientProvider.getIfUnique();
			if (authClient == null) {
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				return false;
			}
			Response<ValidateTokenDto> isValidToken = authClient.validateJwtToken(jwtAuthToken); //authClient.validateJwtToken(jwtAuthToken)

			if (isValidToken == null || isValidToken.getData() == null
					|| isValidToken.getData().getValidated() == null || !isValidToken.getData().getValidated()) {
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				return false;
			}

			return true;
		} catch (FeignException e) {
			log.error("Error Auth Feign occured :  : " + e.getMessage());
		} catch (Exception e) {
			log.error("Error occured while authenticating request : " + e.getMessage());
		}

		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		return false;
	}

//	@Override
//	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
//			ModelAndView modelAndView) throws Exception {
//
//		log.info("[Inside POST Handle Interceptor]" + request.getRequestURI());
//
//	}

}
