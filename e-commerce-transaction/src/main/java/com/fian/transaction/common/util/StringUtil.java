package com.fian.transaction.common.util;

import java.util.List;

public class StringUtil {
    public static boolean matchesTextInPrefixList(String text, List<String> prefix) {
        if (text == null || prefix == null || prefix.isEmpty()) {
            return false;
        }
        for (String pr : prefix) {
            if (text.startsWith(pr)) return true;
        }
        return false;
    }
}
