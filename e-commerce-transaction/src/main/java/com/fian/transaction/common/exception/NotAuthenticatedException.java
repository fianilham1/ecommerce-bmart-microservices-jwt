package com.fian.transaction.common.exception;

public class NotAuthenticatedException extends Exception{
    private String message;

    public NotAuthenticatedException(String message) {
        super(message);
        this.message = message;
    }
}
