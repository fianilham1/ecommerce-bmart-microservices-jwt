package com.fian.transaction.common.exception;

import com.fian.shared.data.entity.Product;

public class NotEnoughProductException extends Exception {

    private static final String DEFAULT_MESSAGE = "Not enough products in stock";

    public NotEnoughProductException() {
        super(DEFAULT_MESSAGE);
    }

    public NotEnoughProductException(Product product) {
        super(String.format("Not enough %s products in stock. Only %d left", product.getName(), product.getQuantityInStock()));
    }
}