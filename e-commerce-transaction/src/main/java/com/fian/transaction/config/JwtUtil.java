package com.fian.transaction.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fian.shared.data.dto.user.UserDetailsDto;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JwtUtil {
    @Value("${jwt.secret}")
    private String secret;

    private final ObjectMapper mapper;

    public JwtUtil(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public UserDetailsDto extractUserDetails(String token) {
        Object obj = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().get("user-details");

        if (obj != null) return mapper.convertValue(obj, UserDetailsDto.class);

        return null;
    }


}
