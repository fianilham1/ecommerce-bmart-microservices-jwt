package com.fian.transaction.common.converter;

import com.fian.transaction.common.util.MapperUtil;
import com.fian.shared.data.dto.cart.CartItemsDto;
import com.fian.shared.data.entity.CartItems;
import org.springframework.stereotype.Service;

@Service
public class CartItemsConverter {
    public CartItemsDto toDto(CartItems cartItems){
        return MapperUtil.parse(cartItems, CartItemsDto.class);
    }

    public CartItems toEntity(CartItemsDto cartItemsDto){
        return MapperUtil.parse(cartItemsDto, CartItems.class);
    }
}

