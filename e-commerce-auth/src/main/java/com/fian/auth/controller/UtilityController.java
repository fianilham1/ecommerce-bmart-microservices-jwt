package com.fian.auth.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.Connection;

@Slf4j
@RestController
@RequestMapping("/utility")
public class UtilityController {

    @Autowired
    @Qualifier("bmartDataSource")
    private DataSource dataSource;

    @GetMapping("/db-check")
    public String dbCheck() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            return "Success";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "Error";
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }
}
