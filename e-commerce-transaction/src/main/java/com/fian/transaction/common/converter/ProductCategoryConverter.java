package com.fian.transaction.common.converter;

import com.fian.transaction.common.util.MapperUtil;
import com.fian.shared.data.dto.product_category.ProductCategoryDto;
import com.fian.shared.data.entity.ProductCategory;
import org.springframework.stereotype.Service;

@Service
public class ProductCategoryConverter {
    public ProductCategoryDto toDto(ProductCategory productCategory){
        return MapperUtil.parse(productCategory, ProductCategoryDto.class);
    }

    public ProductCategory toEntity(ProductCategoryDto productCategoryDto){
        return MapperUtil.parse(productCategoryDto, ProductCategory.class);
    }
}

