package com.fian.auth.common.converter;

import com.fian.auth.common.util.MapperUtil;
import com.fian.shared.data.dto.role.RoleDto;
import com.fian.shared.data.entity.Role;
import org.springframework.stereotype.Service;

@Service
public class RoleConverter {
    public RoleDto toDto(Role role){
        return MapperUtil.parse(role, RoleDto.class);
    }

    public Role toEntity(RoleDto roleDto){
        return MapperUtil.parse(roleDto, Role.class);
    }
}
