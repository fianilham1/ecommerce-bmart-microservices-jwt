package com.fian.transaction.controller;

import com.blibli.oss.common.response.Response;
import com.blibli.oss.common.response.ResponseHelper;
import com.fian.transaction.common.converter.CartConverter;
import com.fian.transaction.common.converter.CartItemsConverter;
import com.fian.shared.data.dto.cart.CartDto;
import com.fian.shared.data.dto.cart.CartItemsDto;
import com.fian.transaction.service.CartService;
import com.fian.shared.data.entity.Cart;
import com.fian.shared.data.dto.DeleteResponseDto;
import com.fian.transaction.validation.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/cart")
public class CartController extends BaseController{

    @Autowired
    private CartService cartService;

    @Autowired
    private CartConverter cartConverter;

    @Autowired
    private CartItemsConverter cartItemsConverter;

    /**
     ROLE_CUSTOMER ---------------
     */
    @PostMapping
    @Role(name = "customer")
    public Response<CartDto> addCartItem(@RequestBody CartItemsDto cartItemsDto){
        try {
            Cart cart = cartService.addItemToCart(cartItemsConverter.toEntity(cartItemsDto), getUsername(),"addQty");
            return ResponseHelper.ok(cartConverter.toDto(cart));
        } catch (Throwable e) {
            log.error("error add cart item : ",e);
            return (Response<CartDto>) showResponseError(e);
        }
    }

    @PutMapping
    @Role(name = "customer")
    public Response<CartDto> updateCartItem(@RequestBody CartItemsDto cartItemsDto){
        try {
            Cart cart = cartService.addItemToCart(cartItemsConverter.toEntity(cartItemsDto), getUsername(),"replaceQty");
            return ResponseHelper.ok(cartConverter.toDto(cart));
        } catch (Throwable e) {
            log.error("error add cart item : ",e);
            return (Response<CartDto>) showResponseError(e);
        }
    }

    @DeleteMapping("/{id}")
    @Role(name = "customer")
    public Response<DeleteResponseDto> deleteItemCart(@PathVariable("id") String cartItemsProductId) {
        try {
            cartService.deleteItemFromCart(cartItemsProductId, getUsername());
            return ResponseHelper.ok(DeleteResponseDto.builder().deleteMessage("delete cart item product with id "+cartItemsProductId+" is success").build());
        } catch (Throwable e) {
            log.error("error delete cart item product with id {} : {}",cartItemsProductId,e);
            return (Response<DeleteResponseDto>) showResponseError(e);
        }
    }

    @GetMapping
    @Role(name = "customer")
    public Response<CartDto> getItemCart() {
        try {
            Cart cart = cartService.getCartCustomer(getUsername());
            return ResponseHelper.ok(cartConverter.toDto(cart));
        } catch (Throwable e) {
            log.error("error get cart item : ",e);
            return (Response<CartDto>) showResponseError(e);
        }
    }


}
