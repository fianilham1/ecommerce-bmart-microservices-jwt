package com.fian.auth.common.converter;

import com.fian.auth.common.util.MapperUtil;
import com.fian.shared.data.dto.user.UserDto;
import com.fian.shared.data.entity.Users;
import org.springframework.stereotype.Service;

@Service
public class UserConverter {
    public UserDto toDto(Users users, String token){
        return UserDto.builder()
                .name(users.getName())
                .username(users.getUsername())
                .role(users.getRole().getName())
                .mobilePhone(users.getMobilePhone())
                .token(token)
                .image(users.getImage())
                .build();
    }

    public UserDto toDto(Users users){
        return UserDto.builder()
                .name(users.getName())
                .username(users.getUsername())
                .role(users.getRole().getName())
                .mobilePhone(users.getMobilePhone())
                .image(users.getImage())
                .build();
    }

    public Users toEntity(Object userDto){
        return MapperUtil.parse(userDto, Users.class);
    }
}
