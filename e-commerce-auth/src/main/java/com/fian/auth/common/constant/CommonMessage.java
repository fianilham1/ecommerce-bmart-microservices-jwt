package com.fian.auth.common.constant;

public interface CommonMessage {

    public static final String ERROR                = "There is an error! Please call the Administrator";
    public static final String NOT_FOUND            = "Data not found!";
    public static final String FOUND                = "Data found!";
    public static final String NOT_DELETED          = "Data failed to delete!";
    public static final String NOT_SAVED            = "Data failed to save";
    public static final String SAVED                = "Data saved successfully!";
    public static final String DELETED              = "Data deleted successfully!";
    public static final String UPDATED              = "Data update successfully!";
    public static final String NOT_UPDATED          = "Data failed to update!";
    public static final String ORDER_OK             = "Order is success! Please proceed to payment..";
    public static final String ORDER_ERROR          = "Order failed to proceed!";
    public static final String ORDER_OUT            = "Product is out of stock/not enough of stock!!";
    public static final String USERNAME_UNAVAILABLE = "Username has been used! Please use the others";
    public static final String USERNAME_AVAILABLE   = "Username can be used!";
    public static final String ACTIVATED            = "Data berhasil diaktifkan! Kamu akan segera diarahkan ke halaman login CIA";
    public static final String NOT_ACTIVATED        = "Data tidak berhasil diaktifkan! Silahkan hubungi administrator untuk kirim ulang email verifikasi";
    public static final String OK                   = "Success!";
    public static final String USERNAME_NOT_FOUND   = "Username not found!";
    public static final String PASSWORD_WRONG           = "Password is wrong!";
    public static final String USERNAME_INVALID_FORMAT  = "Invalid format username";
    public static final String PASSWORD_INVALID_FORMAT  = "Invalid format password, must contain at least 1 uppercase, 1 digit, 1 special char, 8 char length";
    public static final String ROLE_UNAVAILABLE     = "Role is not found!";
    public static final String PASSWORD_UPDATED     = "Your password successfully updated.";

}
