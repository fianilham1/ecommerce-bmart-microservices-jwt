package com.fian.transaction.controller;

import com.blibli.oss.common.response.Response;
import com.blibli.oss.common.response.ResponseHelper;
import com.fian.transaction.common.converter.ProductReviewConverter;
import com.fian.shared.data.dto.product_review.ProductReviewDto;
import com.fian.transaction.service.ProductReviewService;
import com.fian.shared.data.entity.ProductReview;
import com.fian.transaction.validation.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductReviewController extends BaseController{

    @Autowired
    private ProductReviewService productReviewService;

    @Autowired
    private ProductReviewConverter converter;

    /**
     * ROLE_CUSTOMER -----------------
     * */

    @PostMapping("/review")
    @Role(name = "customer")
    public Response<ProductReviewDto> addProductReview(@RequestBody ProductReviewDto productReviewDto){
        try {
            ProductReview productReview = productReviewService.addProductReview(converter.toEntity(productReviewDto),productReviewDto.getProductId(),getUsername());
            return ResponseHelper.ok(converter.toDto(productReview));
        } catch (Throwable e) {
            log.error("error create product categories : ",e);
            return (Response<ProductReviewDto>) showResponseError(e);
        }
    }


}
