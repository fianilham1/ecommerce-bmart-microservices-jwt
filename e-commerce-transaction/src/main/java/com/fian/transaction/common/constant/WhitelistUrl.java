package com.fian.transaction.common.constant;

public interface WhitelistUrl {

    String[] WHITE_LIST_GET_PREFIX_URLS = {
            "/products",
            "/product-category"
    };

}
