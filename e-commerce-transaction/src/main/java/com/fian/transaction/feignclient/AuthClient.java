package com.fian.transaction.feignclient;

import com.blibli.oss.common.response.Response;
import com.fian.shared.data.dto.user.ValidateTokenDto;
import feign.RetryableException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "auth-service", url = "${rest.auth.basepath:http://localhost}")
public interface AuthClient {

    @GetMapping("/token/validate")
    Response<ValidateTokenDto> validateJwtToken(@RequestParam(name = "token") String token) throws RetryableException;

}
