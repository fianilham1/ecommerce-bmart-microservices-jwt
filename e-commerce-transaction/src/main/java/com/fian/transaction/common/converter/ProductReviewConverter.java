package com.fian.transaction.common.converter;

import com.fian.shared.data.dto.product_review.ProductReviewDto;
import com.fian.shared.data.entity.ProductReview;
import org.springframework.stereotype.Service;

@Service
public class ProductReviewConverter {
    public ProductReviewDto toDto(ProductReview productReview){
        return ProductReviewDto.builder()
                .id(productReview.getId())
                .createdDate(productReview.getCreatedDate())
                .productId(productReview.getProduct().getId())
                .userName(productReview.getUsers().getUsername())
                .comment(productReview.getComment())
                .build();
    }

    public ProductReview toEntity(ProductReviewDto productReviewDto){
        return ProductReview.builder()
                .rating(productReviewDto.getRating())
                .comment(productReviewDto.getComment())
                .build();
    }
}

