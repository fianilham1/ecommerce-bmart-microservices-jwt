package com.fian.auth;

import com.fian.shared.PersistentAutoConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication(scanBasePackages =  { "com.fian.auth",
		"com.fian.shared.data.entity",
		"com.fian.shared.repository" })
@Import({PersistentAutoConfig.class})
public class ECommerceUserApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(ECommerceUserApplication.class, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
