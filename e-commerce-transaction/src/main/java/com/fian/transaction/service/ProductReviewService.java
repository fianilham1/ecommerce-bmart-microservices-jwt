package com.fian.transaction.service;

import com.fian.shared.data.entity.Users;
import com.fian.shared.data.entity.web_query.ProductReviewParamsQuery;
import com.fian.shared.data.entity.Product;
import com.fian.shared.data.entity.ProductReview;
import org.springframework.data.domain.Page;


public interface ProductReviewService {

    ProductReview addProductReview(ProductReview productReview, String productId, String userName);
    Page<ProductReview> getAllProductReview(ProductReviewParamsQuery productReviewParamsQuery);
    Page<ProductReview> getAllProductReviewByProduct(ProductReviewParamsQuery productReviewParamsQuery, Product product);
    Page<ProductReview> getAllProductReviewByUsers(ProductReviewParamsQuery productReviewParamsQuery, Users users);

}
