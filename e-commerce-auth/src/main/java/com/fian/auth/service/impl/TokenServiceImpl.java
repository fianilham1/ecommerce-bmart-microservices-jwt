package com.fian.auth.service.impl;

import com.fian.auth.config.JwtUtil;
import com.fian.auth.service.TokenService;
import com.fian.shared.data.dto.user.ValidateTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private MyUserDetailsServiceImpl userDetailsService;

    @Override
    public ValidateTokenDto isTokenValid(String token) {
        String userName = jwtUtil.extractUsername(token);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(userName != null && SecurityContextHolder.getContext().getAuthentication() != null)) {
            return null;
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

        return ValidateTokenDto.builder().validated(jwtUtil.validateToken(token, userDetails)).build();
    }
}
