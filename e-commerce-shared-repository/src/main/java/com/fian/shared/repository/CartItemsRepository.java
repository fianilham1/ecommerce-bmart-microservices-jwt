package com.fian.shared.repository;

import com.fian.shared.data.entity.Cart;
import com.fian.shared.data.entity.CartItems;
import com.fian.shared.data.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartItemsRepository extends JpaRepository<CartItems, Long> {
    List<CartItems> findAllByCartAndMarkForDeleteIsFalse(Cart cart);
    Optional<CartItems> findByProductAndCartAndMarkForDeleteIsFalse(Product product, Cart cart);
    Optional<CartItems> findByIdAndCartAndMarkForDeleteIsFalse(String id, Cart cart);
}
