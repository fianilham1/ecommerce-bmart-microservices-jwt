package com.fian.auth.common.constant;

public interface WhitelistUrl {

    String[] WHITE_LIST_URLS = {
            "/auth/signin",
            "/auth/welcome",
            "/auth/register",
            "/auth/forgot-password/**",
            "/auth/google/signin",
            "/token/validate",
            "/utility/db-check",
            "/actuator/**"
    };

    String[] WHITE_LIST_GET_URLS = {
            "/role/**"
    };

}
