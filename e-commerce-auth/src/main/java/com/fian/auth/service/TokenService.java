package com.fian.auth.service;

import com.fian.shared.data.dto.user.ValidateTokenDto;

public interface TokenService {
    ValidateTokenDto isTokenValid(String token);
}
