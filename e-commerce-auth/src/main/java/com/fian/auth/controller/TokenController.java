package com.fian.auth.controller;

import com.blibli.oss.common.response.Response;
import com.blibli.oss.common.response.ResponseHelper;
import com.fian.auth.service.TokenService;
import com.fian.shared.data.dto.user.ValidateTokenDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/token")
public class TokenController extends BaseController{

    @Autowired
    private TokenService tokenService;

    @GetMapping("/validate")
    public Response<ValidateTokenDto> validateToken(@RequestParam(name = "token") String token) {
        if (!StringUtils.hasLength(token)) return ResponseHelper.badRequest(new HashMap<>());
        try {
            return ResponseHelper.ok(tokenService.isTokenValid(token));
        } catch (Throwable e) {
            log.error("error validate token : ",e);
            return (Response<ValidateTokenDto>) showResponseError(e);
        }
    }

}
