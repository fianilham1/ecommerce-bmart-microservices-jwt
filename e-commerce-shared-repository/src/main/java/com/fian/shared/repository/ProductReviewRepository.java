package com.fian.shared.repository;

import com.fian.shared.data.entity.Product;
import com.fian.shared.data.entity.ProductReview;
import com.fian.shared.data.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductReviewRepository extends JpaRepository<ProductReview, Long> {

    Page<ProductReview> findAllByMarkForDeleteIsFalse(Pageable pageable);
    Page<ProductReview> findAllByProductMarkForDeleteIsFalse(Pageable pageable, Product product );
    Page<ProductReview> findAllByUsersMarkForDeleteIsFalse(Pageable pageable, Users users);
    Optional<ProductReview> findByIdAndMarkForDeleteIsFalse(String id);
}
