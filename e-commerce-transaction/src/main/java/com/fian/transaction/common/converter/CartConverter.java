package com.fian.transaction.common.converter;

import com.fian.transaction.common.util.MapperUtil;
import com.fian.shared.data.dto.cart.CartDto;
import com.fian.shared.data.entity.Cart;
import org.springframework.stereotype.Service;

@Service
public class CartConverter {
    public CartDto toDto(Cart cart){
        CartDto cartDto = MapperUtil.parse(cart, CartDto.class);
        cartDto.setTotalProducts(cartDto.getCartItemsList().size());
        return cartDto;
    }

    public Cart toEntity(CartDto cartDto){
        return MapperUtil.parse(cartDto, Cart.class);
    }
}

