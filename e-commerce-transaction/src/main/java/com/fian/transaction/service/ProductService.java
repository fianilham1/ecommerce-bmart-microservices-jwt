package com.fian.transaction.service;

import com.fian.shared.data.entity.web_query.ProductParamsQuery;
import com.fian.shared.data.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {
    List<Product> addProducts(List<Product> productListRequest);

    Page<Product> getAllProduct(ProductParamsQuery productParamsQuery);

    Product getProductById(String id);

    void deleteProductById (String id);

    Product updateProduct (Product updatedProduct);

    Product updateProductImage (Product updatedProduct);

}
